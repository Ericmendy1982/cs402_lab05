.data 0x10000000
user1:    .word 0
msg1:     .asciiz "Please enter an integer number:"
msg2:     .asciiz "If bytes were layed in reverse order the number would be:"

.text
.globl main
main:   add $sp, $sp, -4
        sw $ra,4($sp)
        la $a0,msg1                         #syscall to input an integer
        li $v0,4
        syscall
        
        li $v0,5                            # syscall to read the integer
        syscall
        
        sw $v0, user1                       
        la $a0,user1
        jal Reverse_bytes                   # call for the procedure to reverse the integer
        la $a0, msg2
        li $v0,4
        syscall
        
        lw $a0, user1                      # print the reversed integer
        li $v0,1
        syscall
        
        lw $ra,4($sp)
        add $sp,$sp,4
        jr $ra

Reverse_bytes:                             # the procedure to reverse the integer
        lb $t0, 0($a0)
        lb $t1, 1($a0)
        lb $t2, 2($a0)
        lb $t3, 3($a0)
        sb $t3, 0($a0)
        sb $t2, 1($a0)
        sb $t1, 2($a0)
        sb $t0, 3($a0)
        jr $ra
        