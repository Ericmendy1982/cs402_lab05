.data 0x10000000


word1:      .word 0x89abcdef      # reserve space for a word


            .text
            .globl main 

main:        la $v0, word1
             lwl $t0,0($v0)
             lwl $t1,1($v0)
             lwl $t2,2($v0)
             lwl $t3,3($v0)
             jr $ra                # return from main