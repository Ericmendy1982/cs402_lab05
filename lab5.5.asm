.data 0x10000000
char1:      .byte 'a'             # reserve space for a byte
double1:    .double 1.1           # reserve space for a double
char2:      .byte 'b'             # b is 0x62 in ASCII
half1:      .half 0x8001          # reserve space for a half-word (2 bytes) 
char3:      .byte 'c'             # c is 0x63 in ASCII
word1:      .word 0x56789abc      # reserve space for a word
char4:      .byte 'd'             # d is 0x64 in ASCII
word2:      .word 0               # reserve a word space for word2

            .text
            .globl main 

main:       la $v0, word1
            lb $t0,0($v0)
            lb $t1,1($v0)
            lb $t2,2($v0)
            lb $t3,3($v0)
            lbu $t4,0($v0)
            lbu $t5,1($v0)
            lbu $t6,2($v0)
            lbu $t7,3($v0)
            lh $t8, half1
            lhu $t9,half1
            la $v1, word2
            sb $t3, 0($v1)
            sb $t2, 1($v1)
            sb $t1, 2($v1)
            sb $t0, 3($v1)
             jr $ra                # return from main
            