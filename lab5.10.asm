
            .data 0x10000000
            .align 0
char1:      .byte 'a'             # reserve space for a byte
word1:      .word 0x89abcdef      # reserve space for a word
char2:      .byte 'b'             # b is 0x62 in ASCII
word2:      .word 0      # reserve space for a word

            .text
            .globl main 
main:       lui $1,4096      #load address of word1 and word2 to $4, $5
            ori $4,$1,1
            ori $5,$1,6
            
                            
            lwl $8, 3($4)      # load word1 to register $t0
            lwr $8, 0($4)
            swl $8, 3($5)      # copy $t0 to word2
            swr $8, 0($5)
            
                    
            jr $31                # return from main
      
 
