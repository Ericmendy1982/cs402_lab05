.data 0x10000000


word1:      .word 0x89abcdef      # reserve space for a word


            .text
            .globl main 

main:        la $v0, word1
             lwr $t4,0($v0)
             lwr $t5,1($v0)
             lwr $t6,2($v0)
             lwr $t7,3($v0)
             jr $ra                # return from main
